from bs4 import BeautifulSoup
from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as ec
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.common.by import By
import urllib.parse
import requests
from lxml import etree
import advertools as adv
import pymongo
from seoanalyzer import analyze
from datetime import datetime
import os
from googlesearch import search
import sys

connection = pymongo.MongoClient('mongodb://localhost:27017')
database = connection['seoanalyzer']
collection = database['sites']
os.environ['TZ'] = 'Asia/Jakarta'
searchKeyword = sys.argv[2]
searchType = sys.argv[1]
readSitemap = False

headers = {
    'Access-Control-Allow-Origin': '*',
    'Access-Control-Allow-Methods': 'GET',
    'Access-Control-Allow-Headers': 'Content-Type',
    'Access-Control-Max-Age': '3600',
    'User-Agent': 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:52.0) Gecko/20100101 Firefox/52.0'
}
timeout_in_seconds = 10

def readXML(url):
    xmlDict = []
    try:
        file = requests.get(url)
        root = etree.fromstring(file.content)

        xml = file.text

        soup = BeautifulSoup(xml, 'lxml')
        sitemapTags = soup.find_all("sitemap")

        for sitemap in root:
            children = sitemap.getchildren()
            data = {}
            data['url'] = children[0].text
            data['changeFreq'] = children[1].text

            xmlDict.append(data)
    except Exception as e:
        pass

    return xmlDict

def scrapSite():
    tagA = []
    hosts = []

    if searchType == 'url':
        url = searchKeyword
        options = webdriver.ChromeOptions()
        options.add_argument('--headless')
        driver = webdriver.Chrome(chrome_options=options)
        driver.get(url)

        html = driver.page_source

        soup = BeautifulSoup(html, 'lxml')
        search = soup.find('div', {'id': 'search'})
        tagA = search.find_all('a')
    elif searchType == 'keyword':
        tagA = searchGoogle()
    else:
        print('search type undefined')
        return True

    for a in tagA:
        parsed_uri = None

        if searchType == 'url':
            parsed_uri = urllib.parse.urlsplit(a['href'])
        elif searchType == 'keyword':
            parsed_uri = urllib.parse.urlsplit(a)

        if parsed_uri.netloc == 'translate.google.com':
            continue

        if parsed_uri.hostname not in hosts:
            uri = None
            isUriValid = False

            try:
                uri = parsed_uri.scheme + '://' + parsed_uri.hostname
                isUriValid = True
            except Exception as e:
                print(str(e))

            if not isUriValid:
                continue

            hosts.append(parsed_uri.hostname)

            print(parsed_uri.hostname)

            dataSite = {}
            dataSite['datetime'] = datetime.now()
            dataSite['site'] = uri
            dataSite['hostname'] = parsed_uri.hostname
            dataSite['url'] = None
            dataSite['search'] = searchKeyword
            dataSite['search_type'] = searchType

            if searchType == 'url':
                dataSite['url'] = a['href']
            elif searchType == 'keyword':
                dataSite['url'] = a

            dataSite['sitemap'] = []
            dataSite['keywords'] = []

            xmls = [
                uri + '/post-sitemap.xml',
                uri + '/sitemap.xml',
                uri + '/sitemap-index.xml',
                uri + '/sitemapindex.xml',
                uri + '/sitemap/sitemap.xml',
                uri + '/sitemap/index.xml',
                uri + '/sitemap1.xml',
                uri + '/page-sitemap.xml'
            ]

            if readSitemap:
                for x in xmls:
                    try:
                        XMLData = readXML(x)
                    except Exception as e:
                        XMLData = []

                    if len(XMLData) > 0:
                        dataSitemap = {}
                        dataSitemap['sitemap'] = x
                        dataSitemap['links'] = []

                        with open('sitemaps/' + parsed_uri.hostname + '.txt', 'a+', encoding='utf8') as f:
                            for i in XMLData:
                                f.write(i['url'] + '\n')
                                dataSitemap['links'].append(i['url'])

                        dataSite['sitemap'].append(dataSitemap)

                    print(x)
                    print(str(len(XMLData)))
                    print('=====================')

            if searchType == 'url':
                outputAnalyze = analyze(url=a['href'], follow_links=False)
            elif searchType == 'keyword':
                outputAnalyze = analyze(url=a, follow_links=False)

            if 'keywords' in outputAnalyze:
                dataSite['keywords'] = outputAnalyze['keywords']

            collection.insert_one(dataSite)

            print(uri)

def searchGoogle():
    urls = []
    googling = search(searchKeyword, tld="com", num=10, stop=10, pause=2)

    for a in googling:
        urls.append(a)

    return urls

scrapSite()