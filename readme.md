PYTHON SEOANALYZER

requirement
1. rust
2. selenium
3. chromedriver 

installation
1. download rust https://www.rust-lang.org/tools/install
2. set rust env variable in ~/.cargo/bin
3. upgrade pip to the latest version
4. pip install selenium
5. download chromedriver https://sites.google.com/chromium.org/driver/downloads?authuser=0
6. set chromedriver env variable in path environment variables

jika persyaratan diatas sudah terpenuhi, eksekusi perintah berikut :

pip install -r requirements.txt

perintah diatas digunakan untuk menginstall semua package yang ada di file requirements.txt
jangan lupa untuk membuat virtual env dulu dengan cara :

python -m venv venv

dari direktori root project